# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.
task_type = (
    ("Active", "Active"),
    ("Inactive", "Inactive")
)


class Task(models.Model):
    id = models.IntegerField(primary_key=True)
    task_name = models.CharField(max_length=50, null=False, blank=True)
    task_status = models.CharField(
        max_length=100, choices=task_type, default="Active")
   
    def save(self, *args, **kwargs):
        return super(Task, self).save(*args, **kwargs)

    def __unicode__(self):
        return "{}:{}".format(self.task_name, self.task_status)
