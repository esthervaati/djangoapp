# How to Dockerise python applications


### Build image

sudo docker build -t django_application_image .


### Run app

sudo docker run -p 8000:8000 -i -t django_application_image



API Routes
====================

### Create Task

**POST:**
```
/tasks/tasks/
```

**Body:**
```json
{
    "task_name": "Learn React",
    "task_status": "Active"
}
```

**Response:**
```json
HTTP 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "task_name": "Learn React",
    "task_status": "Active"
}
```














